package net;


public class Connection 
{
    public int targetLayer;
    
    public int targetNumber;
    
    public double weight;
    public double deltaWeight;
    
    public Connection(int connectionLayer, int connectionNumber)
    {
        targetLayer = connectionLayer;
        targetNumber = connectionNumber;
    
        weight = randWeight();
        deltaWeight = 0;
    }
    
    public double randWeight()
    {
        return Math.random();
    }
    
    public void setWeight(double weight)
    {
        this.weight = weight;
    }
    
    public void setDeltaWeight(double weight)
    {
        this.deltaWeight = weight;
    }
    
    public double getWeight()
    {
        return weight;
    }

}
