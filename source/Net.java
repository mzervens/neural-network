
package net;

import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.List;


public class Net 
{
    public List<List<Neuron>> neuronLayers;
    
    public double error;

    public Net(Integer... args)
    {
        neuronLayers = new ArrayList<>();
        
        for(int i = 0; i < args.length; i++)
        {
            int neuronCount = args[i];
            List<Neuron> layerNeurons = new ArrayList<>();
            int nLayerNeurs;
            int nLayer;
            
            if(i + 1 >= args.length)
            {
                nLayerNeurs = nLayer = 0;
            }
            else
            {
                nLayerNeurs = args[i + 1];
                nLayer = i + 1;
            }
            
            for(int n = 0; n < neuronCount; n++)
            {
                layerNeurons.add(n, new Neuron(nLayerNeurs, nLayer));
            }
            
            neuronLayers.add(i, layerNeurons);
        }
        
        setNeuronInputCons();
    
    }
    
    private void setNeuronInputCons()
    {
        for(int i = 1; i < neuronLayers.size(); i++)
        {
            for(int s = 0; s < neuronLayers.get(i).size(); s++)
            {
                Neuron n = neuronLayers.get(i).get(s);
                
                for(int m = 0; m < neuronLayers.get(i - 1).size(); m++)
                {
                    Neuron n2 = neuronLayers.get(i - 1).get(m);
                    for(Connection c : n2.connections)
                    {
                        if(c.targetLayer == i && c.targetNumber == s)
                        {
                            Connection c2 = new Connection(i - 1, m);
                            c2.setWeight(c.getWeight());
                            n.addInputConnection(c2);
                        
                        }
                        
                    }

                }
                
                neuronLayers.get(i).set(s, n);
            }
        
        }
    }
    
    public void feedForward(Double... args) throws Exception
    {
        if(args.length != neuronLayers.get(0).size())
        {
            throw new InvalidParameterException();
        }
        
        for(int i = 0; i < args.length; i++)
        {
            Neuron n = neuronLayers.get(0).get(i);
            n.setOutput(args[i]);
        
            neuronLayers.get(0).set(i, n);
        }
        
        for(int i = 1; i < neuronLayers.size(); i++)
        {
            for(int n = 0; n < neuronLayers.get(i).size(); n++)
            {
                double sum = 0;
                for(Connection c : neuronLayers.get(i).get(n).connected)
                {
                    Neuron neur = neuronLayers.get(c.targetLayer).get(c.targetNumber);
                    sum += neur.getOutput() * c.weight;
                }
            
                
                sum += neuronLayers.get(i).get(n).getBias();

                neuronLayers.get(i).get(n).generateOutput(sum);
            }
        
        }
    
    }
    
    public void backPropagate(double learningCoef, double steepness, Double... args) throws Exception
    {
        int oLayerInd = neuronLayers.size() - 1;
        
        if(args.length != neuronLayers.get(oLayerInd).size())
        {
            throw new InvalidParameterException();
        }
        // Calculate the overal output error
        // Using Root mean square error
        double rmsError = 0;
        for(int i = 0; i < neuronLayers.get(oLayerInd).size(); i++)
        {
            double neuronError = args[i] - neuronLayers.get(oLayerInd).get(i).getOutput();
            neuronError *= neuronError;
            
            rmsError += neuronError;
        }
        
        rmsError /= neuronLayers.get(oLayerInd).size();
        rmsError = Math.sqrt(rmsError);
        
        error = rmsError;
        
        // Calculate the output layer gradients
        for(int i = 0; i < neuronLayers.get(oLayerInd).size(); i++)
        {
            neuronLayers.get(oLayerInd).get(i).calculateGradient(args[i]);
        }
        
        
        // Calculate the hidden layer gradients that depend on the grandients of layer neurons that have gradients already calculated
        for(int i = oLayerInd - 1; i > 0; i--)
        {
            for(int l = 0; l < neuronLayers.get(i).size(); l++)
            {
                Neuron n = neuronLayers.get(i).get(l);
                double contributedSums = 0;
                for(Connection c : n.connections)
                {
                    contributedSums += c.getWeight() * neuronLayers.get(c.targetLayer).get(c.targetNumber).getOutputGradient();
                }
                
                n.calculateGradientIfHidden(contributedSums);
                neuronLayers.get(i).set(l, n);       
            }
        }
        
        // Update the weights and biases
        
        for(int i = oLayerInd; i > 0; i--)
        {
            for(int n = 0; n < neuronLayers.get(i).size(); n++)
            {
                neuronLayers.get(i).get(n).updateBias(learningCoef);
                for(int c = 0; c < neuronLayers.get(i).get(n).connected.size(); c++)
                {
                    Connection con = neuronLayers.get(i).get(n).connected.get(c);
                    double oldWeight = con.weight;

                    Neuron inputNeur = neuronLayers.get(con.targetLayer).get(con.targetNumber);
                    
                  //  double newDelta = learningCoef * inputNeur.getOutput() * neuronLayers.get(i).get(n).getOutputGradient() + oldDelta * steepness;
                    double newDelta = oldWeight + learningCoef * 1 * inputNeur.getOutput() * neuronLayers.get(i).get(n).getOutputGradient();
                    con.setWeight(newDelta);
                    con.setDeltaWeight(newDelta);
                    
                    neuronLayers.get(i).get(n).connected.set(c, con);
                    
                    for(int cc = 0; cc < inputNeur.connections.size(); cc++)
                    {
                        Connection c2 = inputNeur.connections.get(cc);
                        if(c2.targetLayer == i && c2.targetNumber == n)
                        {
                            c2.setWeight(newDelta);
                            c2.setDeltaWeight(newDelta);
                            
                            inputNeur.connections.set(cc, c2);
                            break;
                        }
                    }
                    
                    neuronLayers.get(con.targetLayer).set(con.targetNumber, inputNeur);
                    
                }
            }
        
        }
    
    
    }
    
    public Double[] getResultVector()
    {
        Double vector [] = new Double[neuronLayers.get(neuronLayers.size() - 1).size()]; 
        for(int i = 0; i < neuronLayers.get(neuronLayers.size() - 1).size(); i++)
        {
            vector[i] = neuronLayers.get(neuronLayers.size() - 1).get(i).getOutput();
        }
        
        return vector;
    }
    
    public void printNet()
    {
        for(int i = 0; i < neuronLayers.size(); i++)
        {
            System.out.println("Layer number " + i);
            
            for(int n = 0; n < neuronLayers.get(i).size(); n++)
            {
                System.out.println("Neuron #" + n);
                System.out.println("Neuron bias is " + neuronLayers.get(i).get(n).getBias());
                System.out.println(" Output connections:");
                for(Connection c : neuronLayers.get(i).get(n).connections)
                {
                    System.out.println(c.targetLayer + " " + c.targetNumber + " weight: " + c.weight);
                
                }
                System.out.println(" Input connections:");
                for(Connection c : neuronLayers.get(i).get(n).connected)
                {
                    System.out.println(c.targetLayer + " " + c.targetNumber + " weight: " + c.weight);
                
                }
                
            }
        
        }
    
    }
    
    

}
