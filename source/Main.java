
package net;

import java.io.ByteArrayInputStream;
import java.lang.reflect.Array;
import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;


public class Main 
{
    public static int performance = 0;
    public static int samples = 0;
    
    public static void main(String args []) throws Exception
    {
        double learningRate = 0.45;
        double steepness = 0.3;
        Net n = new Net(784, 30, 10);

        
        /*
        List<Double[]> inputs = new ArrayList<>();
        inputs.add(new Double[] {1.0, 1.0});
        inputs.add(new Double[] {1.0, 0.0});
        inputs.add(new Double[] {0.0, 1.0});
        inputs.add(new Double[] {0.0, 0.0});
        inputs.add(new Double[] {0.0, 1.0});
        inputs.add(new Double[] {1.0, 1.0});
        inputs.add(new Double[] {0.0, 1.0});
        inputs.add(new Double[] {0.0, 0.0});
        inputs.add(new Double[] {1.0, 0.0});
        inputs.add(new Double[] {1.0, 1.0});
        
        List<Double[]> outputs = new ArrayList<>();
        outputs.add(new Double[] {1.0});
        outputs.add(new Double[] {0.0});
        outputs.add(new Double[] {0.0});
        outputs.add(new Double[] {0.0});
        outputs.add(new Double[] {0.0});
        outputs.add(new Double[] {1.0});
        outputs.add(new Double[] {0.0});
        outputs.add(new Double[] {0.0});
        outputs.add(new Double[] {0.0});
        outputs.add(new Double[] {1.0});
        
        List<Double[]> inputsXOR = new ArrayList<>();
        inputsXOR.add(new Double[] {1.0, 1.0});
        inputsXOR.add(new Double[] {1.0, 0.0});
        inputsXOR.add(new Double[] {0.0, 1.0});
        inputsXOR.add(new Double[] {0.0, 0.0});
        inputsXOR.add(new Double[] {0.0, 1.0});
        inputsXOR.add(new Double[] {1.0, 1.0});
        inputsXOR.add(new Double[] {0.0, 1.0});
        inputsXOR.add(new Double[] {0.0, 0.0});
        inputsXOR.add(new Double[] {1.0, 0.0});
        inputsXOR.add(new Double[] {1.0, 1.0});
        
        List<Double[]> outputsXOR = new ArrayList<>();
        outputsXOR.add(new Double[] {0.0});
        outputsXOR.add(new Double[] {1.0});
        outputsXOR.add(new Double[] {1.0});
        outputsXOR.add(new Double[] {0.0});
        outputsXOR.add(new Double[] {1.0});
        outputsXOR.add(new Double[] {0.0});
        outputsXOR.add(new Double[] {1.0});
        outputsXOR.add(new Double[] {0.0});
        outputsXOR.add(new Double[] {1.0});
        outputsXOR.add(new Double[] {0.0});
        
        
        for(int i = 0; i < 10000; i++)
        {
            train(n, learningRate, steepness, inputsXOR, outputsXOR);
        }
        
        n.feedForward(1.0, 1.0);
        System.out.println("1.0, 1.0");
        
        printResults(n);
        
        n.feedForward(1.0, 0.0);
        System.out.println("1.0, 0.0");
        
        printResults(n);
        
        n.feedForward(0.0, 1.0);
        System.out.println("0.0, 1.0");
        
        printResults(n);
        
        n.feedForward(0.0, 0.0);
        System.out.println("0.0, 0.0");
        
        printResults(n);


        */
        
        byte[] numPics = readBinary("src/net/train-images.idx3-ubyte");
        byte[] picLabels = readBinary("src/net/train-labels.idx1-ubyte");

        ByteBuffer wrap = ByteBuffer.wrap(numPics);
        ByteBuffer wrapLabels = ByteBuffer.wrap(picLabels);
        
        wrapLabels.getInt();
        wrapLabels.getInt();
        
        wrap.getInt();
        int trainingSize = wrap.getInt();
        int numRows = wrap.getInt();
        int numCols = wrap.getInt();
        
        System.out.println("training size " + trainingSize + " rows " + numRows + " cols " + numCols);
        List<Double[]> inputs = new ArrayList<>();

        for(int i = 0; i < trainingSize / 6; i++)
        {
            Double [] picturePix = new Double[numRows * numCols];
            for(int s = 0; s < numRows * numCols; s++)
            {
                picturePix[s] = (double) (wrap.get() & 0xFF);
            }
            picturePix = convertData(picturePix, 255, 0, 1, 0);
            inputs.add(picturePix);

        }
        
        List<Double[]> outputs = new ArrayList<>();

        for(int i = 0; i < trainingSize / 6; i++)
        {
            int label = (wrapLabels.get() & 0xFF);
            Double [] pictureLabel = convertLabelData(label);
            outputs.add(pictureLabel);
        }
        
        printImage(inputs.get(0));
        
        System.out.println("");
        printLabelVector(outputs.get(0));
        
        for(int i = 0; i < 30; i++)
        {
            train(n, learningRate, steepness, inputs, outputs);
            System.out.println("Epoch #" + i + " finished!");
            System.out.println("Correct answer percentage: " + ((performance * 100) / samples) + "%");
        }
        
        
        testPerformance(inputs, outputs, n);
        
        
       // printResults(n);
        
       // System.out.println((int) numPics[202] & 0xFF);
    }
    
    public static void train(Net n, double learningRate, double steepness, List<Double[]> inputs, List<Double[]> outputs) throws Exception
    {
        samples = 0;
        performance = 0;
        
        for(int i = 0; i < inputs.size(); i++)
        {
            n.feedForward(inputs.get(i));

            n.backPropagate(learningRate, steepness, outputs.get(i));
  
            if(gradeOutput(n.getResultVector(), outputs.get(i)))
            {
                performance++;
            }
            
            samples++;
        }

    }
    
    public static boolean gradeOutput(Double[] res, Double[] expec)
    {
        boolean correct = true;
        int resMax = 0;
        int expMax = 0;
        for(int i = 0; i < res.length; i++)
        {
            if(res[i] >= res[resMax])
            {
                resMax = i;
            }
            if(expec[i] >= expec[expMax])
            {
                expMax = i;
            }
            
        
        }
        
        if(resMax != expMax)
        {
            correct = false;
        }
        
        return correct;
    }
    
    public static void printResults(Net n)
    {
       // n.printNet();
        
        System.out.println("");

        for(int i = 0; i < n.neuronLayers.get(n.neuronLayers.size() - 1).size(); i++)
        {
            System.out.print(String.format( "%.2f", n.neuronLayers.get(n.neuronLayers.size() - 1).get(i).getOutput()) + " ");
        }
        
        System.out.println("");
        
    }
    
    public static byte[] readBinary(String path) throws Exception
    {
        Path fPath = Paths.get(path);
        return Files.readAllBytes(fPath);
    }
    
    
    public static Double[] convertData(Double [] data, double oldMax, double oldMin, double newMax, double newMin)
    {
        double oldRange = oldMax - oldMin;  
        double newRange = newMax - newMin;  
        
        for(int i = 0; i < data.length; i++)
        {
            data[i] = (((data[i] - oldMin) * newRange) / oldRange) + newMin;
        }
    
        return data;
    }
    
    public static Double[] convertLabelData(int number)
    {
        Double [] vector = new Double[10];
        for(int i = 0; i < vector.length; i++)
        {
            vector[i] = 0.0;
        }
        
        vector[number] = 1.0;

        return vector;
    }
    
    public static void printImage(Double [] img)
    {
        int index = 0;
        
        for(int i = 0; i < 28; i++)
        {
            for(int n = 0; n < 28; n++)
            {
                
                if(img[index] > 0)
                {
                    System.out.print("\u001B[31m" + String.format( "%.2f", img[index]) + "\u001B[0m" + " ");
                }
                else
                {
                    System.out.print(String.format( "%.2f", img[index]) + " ");
                }
                index++;
            }
            System.out.println("");
        
        }
    
    }
    
    public static void printLabelVector(Double [] vec)
    {
        for(int i = 0; i < vec.length; i++)
        {
            System.out.print(vec[i] + " ");
        }
        System.out.println("");
    }
    
    public static void testPerformance(List<Double[]> input, List<Double[]> output, Net n) throws Exception
    {
        n.feedForward(input.get(1));
        printReadable(output, n, 1);
        
        n.feedForward(input.get(3));
        printReadable(output, n, 3);
        
        n.feedForward(input.get(5));
        printReadable(output, n, 5);
        
        n.feedForward(input.get(7));
        printReadable(output, n, 7);
        
        n.feedForward(input.get(2));
        printReadable(output, n, 2);
        
        n.feedForward(input.get(0));
        printReadable(output, n, 0);
        
        n.feedForward(input.get(13));
        printReadable(output, n, 13);
        
        n.feedForward(input.get(15));
        printReadable(output, n, 15);
        
        n.feedForward(input.get(17));
        printReadable(output, n, 17);
        
        n.feedForward(input.get(4));
        printReadable(output, n, 4);
    
    }
    
    public static void printReadable(List<Double[]> output, Net n, int indx)
    {
        System.out.println("The result should be:");
        printLabelVector(output.get(indx));
        
        System.out.println("The result was:");
        printResults(n);
    
    
    }
}
