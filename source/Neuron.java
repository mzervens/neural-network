package net;

import java.util.ArrayList;
import java.util.List;


public class Neuron 
{
    public List<Connection> connections;
    public List<Connection> connected;
    
    private double bias;
    
    private double output;
    
    private double outputGradient;
    
    public Neuron(int connections, int nextLayerNumber)
    {
        bias = randWeight();
        
        this.connections = new ArrayList<>();
        this.connected = new ArrayList<>();
        
        if(connections <= 0)
        {
            return;
        }
        
        
        for(int i = 0; i < connections; i++)
        {
            this.connections.add(new Connection(nextLayerNumber, i));
        }
    
        
    }
    
    public void updateBias(double learningCoef)
    {
        bias = bias + learningCoef * 1 * outputGradient;
    }
    
    public void calculateGradient(double expectedValue)
    {
        //System.out.println("expected value was " + expectedValue + " output was " + this.output);
        double delta = expectedValue - this.output;
        outputGradient = delta * derivativeSigmoidFunc(this.output);
       // outputGradient = output * (1 - output) * delta;
    }
    
    /**
     * Parameter sum represents the value that is created
     * by multiplying connection weights with the gradients of the neurons that are connected to this neuron in the next layer
     * @param sum 
     */
    public void calculateGradientIfHidden(double sum)
    {
      //  outputGradient = sum / 0.5 * derivativeSigmoidFunc(this.output);
        outputGradient = sum * derivativeSigmoidFunc(this.output);
      //  outputGradient = output * (1 - output) * sum;
    }
    
    public double getOutputGradient()
    {
        return outputGradient;
    }
    
    public void modifyOutputConnection(Connection c, int index)
    {
        connections.set(index, c);
    }
    
    public void modifyInputConnection(Connection c, int index)
    {
        connected.set(index, c);
    }
    
    public void addInputConnection(Connection c)
    {
        connected.add(c);
    }
    
    public void setOutput(double input)
    {
        output = input;
    }
    
    public double getOutput()
    {
        return output;
    }
    
    public void setBias(double bias)
    {
        this.bias = bias;
    }
    
    public double getBias()
    {
        return bias;
    }
    
    public void generateOutput(double input)
    {
        output = sigmoidFunction(input);
    }
    
    // ∑ wj * xj −b
    public double sigmoidFunction(double sum)
    {
        return 1 / (1 + Math.exp(-sum));
    }
    
    public double derivativeSigmoidFunc(double sum)
    {
        return sigmoidFunction(sum) * (1 - sigmoidFunction(sum));
    }
    
    public double randWeight()
    {
        return Math.random();
    }

}
